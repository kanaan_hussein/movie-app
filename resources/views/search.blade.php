@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Movies</li>
        </ol>
    </nav>

    <!-- /breadcrumbs -->
    <!-- movie cards -->
    <div class="container">
        <form action="/search" method="POST" role="search">
            {{ csrf_field() }}
            <div class="form-row">
                <div class="col-6 col-md-9 mb-2 mb-md-0">
                    <input type="search" class="form-control form-control-lg" name="search" placeholder="Search ...">
                </div>
                <div class="col-6 col-md-3">
                    <button type="submit" class="btn btn-block btn-sm btn-primary">Search</button>
                </div>
            </div>
        </form><div class="container">


                    <br>
                    <div class="alert alert-primary" role="alert">
                        <i class="fas fa-exclamation-circle"></i> The following movies are currently in theaters.
                    </div>
                    <div class="row">
                        @if(isset($details))
                            <p> The Search results for your query <b> {{ $query }} </b> are :</p>
                        @foreach($details as $movie)
                            <div class="col-sm-6 col-xs-6" style="padding-block-start: 11px">
                                <div class="list mb-2">
                                    <div class="list-header">
                                        <a href="{{route('movie.movie',$movie->id)}}" class="list-header-image">
                                            <img src="{{$movie->img[0]}}">
                                        </a>
                                    </div>
                                    <div class="list-content">
                                        <h2><a href="{{route('movie.movie',$movie->id)}}" class="text-black">{{$movie->name}}</a></h2>
                                        <span class="list-meta">
                    	<span class="list-meta-item"><i class="fas fa-clock"></i> {{$movie->year}}</span>
                                 <input id="input-1" name="input-1" class="rating rating-loading list-meta-item" data-min="0" data-max="5" data-step="0.1" value="{{ $movie->averageRating }}" data-size="xs" disabled="">

                        </span>
                                        <p>{{ \Illuminate\Support\Str::limit($movie->desc, 200, $end='...') }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @endif
                    </div>
                </div>
        </div>

        <br>
{{--        <div class="alert alert-primary" role="alert">--}}
{{--            <i class="fas fa-exclamation-circle"></i> The following movies are currently in theaters.--}}
{{--        </div>--}}
{{--        <div class="row">--}}
{{--            @foreach($movies as $movie)--}}
{{--                <div class="col-sm-6 col-xs-6" style="padding-block-start: 11px">--}}
{{--                    <div class="list mb-2">--}}
{{--                        <div class="list-header">--}}
{{--                            <a href="{{route('movie.movie',$movie->id)}}" class="list-header-image">--}}
{{--                                <img src="{{$movie->img[0]}}">--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="list-content">--}}
{{--                            <h2><a href="{{route('movie.movie',$movie->id)}}" class="text-black">{{$movie->name}}</a></h2>--}}
{{--                            <span class="list-meta">--}}
{{--                    	<span class="list-meta-item"><i class="fas fa-clock"></i> {{$movie->year}}</span>--}}
{{--                                 <input id="input-1" name="input-1" class="rating rating-loading list-meta-item" data-min="0" data-max="5" data-step="0.1" value="{{ $movie->averageRating }}" data-size="xs" disabled="">--}}

{{--                        </span>--}}
{{--                            <p>{{ \Illuminate\Support\Str::limit($movie->desc, 200, $end='...') }}</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            @endforeach--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <!-- /movie cards -->--}}
{{--    <br>--}}
{{--    <!-- Newsletter -->--}}
{{--    <section class="newsletter text-white text-center">--}}
{{--        <div class="overlay"></div>--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-xl-9 mx-auto">--}}
{{--                    <h2 class="mb-4">Love discounts? We do too!</h2>--}}
{{--                </div>--}}
{{--                <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">--}}
{{--                    <form>--}}
{{--                        <div class="form-row">--}}
{{--                            <div class="col-12 col-md-9 mb-2 mb-md-0">--}}
{{--                                <input type="email" class="form-control form-control-lg" placeholder="Enter your email...">--}}
{{--                            </div>--}}
{{--                            <div class="col-12 col-md-3">--}}
{{--                                <button type="submit" class="btn btn-block btn-lg btn-primary">Discounts!</button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
@endsection
@section('scripts')
    <script type="text/javascript">
        $("#input-id").rating();
    </script>
@endsection
