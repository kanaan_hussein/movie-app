@extends('layouts.app')
<link rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
      integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
      crossorigin="anonymous">
<link rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
@section('links')
    <style>
        body {
            margin-top: 20px;
            background-color: #e9ebee;
        }

        .be-comment-block {
            margin-bottom: 50px !important;
            border: 1px solid #edeff2;
            border-radius: 2px;
            padding: 50px 70px;
            border: 1px solid #ffffff;
        }

        .comments-title {
            font-size: 16px;
            color: #262626;
            margin-bottom: 15px;
            font-family: 'Conv_helveticaneuecyr-bold';
        }

        .be-img-comment {
            width: 60px;
            height: 60px;
            float: left;
            margin-bottom: 15px;
        }

        .be-ava-comment {
            width: 60px;
            height: 60px;
            border-radius: 50%;
        }

        .be-comment-content {
            margin-left: 80px;
        }

        .be-comment-content span {
            display: inline-block;
            width: 49%;
            margin-bottom: 15px;
        }

        .be-comment-name {
            font-size: 13px;
            font-family: 'Conv_helveticaneuecyr-bold';
        }

        .be-comment-content a {
            color: #383b43;
        }

        .be-comment-content span {
            display: inline-block;
            width: 49%;
            margin-bottom: 15px;
        }

        .be-comment-time {
            text-align: right;
        }

        .be-comment-time {
            font-size: 11px;
            color: #b4b7c1;
        }

        .be-comment-text {
            font-size: 13px;
            line-height: 18px;
            color: #7a8192;
            display: block;
            background: #f6f6f7;
            border: 1px solid #edeff2;
            padding: 15px 20px 20px 20px;
        }

        .form-group.fl_icon .icon {
            position: absolute;
            top: 1px;
            left: 16px;
            width: 48px;
            height: 48px;
            background: #f6f6f7;
            color: #b5b8c2;
            text-align: center;
            line-height: 50px;
            -webkit-border-top-left-radius: 2px;
            -webkit-border-bottom-left-radius: 2px;
            -moz-border-radius-topleft: 2px;
            -moz-border-radius-bottomleft: 2px;
            border-top-left-radius: 2px;
            border-bottom-left-radius: 2px;
        }

        .form-group .form-input {
            font-size: 13px;
            line-height: 50px;
            font-weight: 400;
            color: #b4b7c1;
            width: 100%;
            height: 50px;
            padding-left: 20px;
            padding-right: 20px;
            border: 1px solid #edeff2;
            border-radius: 3px;
        }

        .form-group.fl_icon .form-input {
            padding-left: 70px;
        }

        .form-group textarea.form-input {
            height: 150px;
        }


    </style>
@endsection

@section('content')
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>

                            <script src='https://js.stripe.com/v2/' type='text/javascript'></script>
                            <form accept-charset="UTF-8" action="/" class="require-validation"
                                  data-cc-on-file="false"
                                  data-stripe-publishable-key="pk_test_yourPublishableKey"
                                  id="payment-form" method="post">
                                {{ csrf_field() }}
                                <div class='form-row'>
                                    <div class='col-xs-12 form-group required'>
                                        <label class='control-label'>Name on Card</label> <input
                                            class='form-control' size='4' type='text'>
                                    </div>
                                </div>
                                <div class='form-row'>
                                    <div class='col-xs-12 form-group card required'>
                                        <label class='control-label'>Card Number</label> <input
                                            autocomplete='off' class='form-control card-number' size='20'
                                            type='text'>
                                    </div>
                                </div>
                                <div class='form-row'>
                                    <div class='col-xs-4 form-group cvc required'>
                                        <label class='control-label'>CVC</label> <input
                                            autocomplete='off' class='form-control card-cvc'
                                            placeholder='ex. 311' size='4' type='text'>
                                    </div>
                                    <div class='col-xs-4 form-group expiration required'>
                                        <label class='control-label'>Expiration</label> <input
                                            class='form-control card-expiry-month' placeholder='MM' size='2'
                                            type='text'>
                                    </div>
                                    <div class='col-xs-4 form-group expiration required'>
                                        <label class='control-label'> </label> <input
                                            class='form-control card-expiry-year' placeholder='YYYY'
                                            size='4' type='text'>
                                    </div>
                                </div>
                                <div class='form-row'>
                                    <div class='col-md-12'>
                                        <div class='form-control total btn btn-info'>
                                            Total: <span class='amount'>$300</span>
                                        </div>
                                    </div>
                                </div>
                                <div class='form-row'>
                                    <div class='col-md-12 form-group'>
                                        <button class='form-control btn btn-primary submit-button'
                                                type='submit' style="margin-top: 10px;">Pay »</button>
                                    </div>
                                </div>
                                <div class='form-row'>
                                    <div class='col-md-12 error form-group hide'>
                                        <div class='alert-danger alert'>Please correct the errors and try
                                            again.</div>
                                    </div>
                                </div>
                            </form>

                        </div>

            </div>
        </div>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
            <li class="breadcrumb-item"><a href="movies.html">Movies</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$movie->name}}</li>
        </ol>
    </nav>
    <!-- /breadcrumbs -->
    <div class="movie-buttons text-center">
        <form action="{{ route('movie.post')}}" method="POST">
            {{ csrf_field() }}

            <div class="rating">
                <input id="input-1" name="rate" class="rating rating-loading" data-min="0" data-max="5"
                       data-step="1" value="{{ $movie->userAverageRating }}" data-size="xs">
                <input type="hidden" name="id" required="" value="{{ $movie->id }}">
                <br/>
                <button class="btn btn-labeled btn-recommend">
                    <span class="btn-label"><i class="fas fa-thumbs-up"></i></span> Submit Review
                </button>
            </div>
        </form>
    </div>
    <div class="movie_card" id="bright">
        <div class="info_section">
            <div class="movie_header">
                <img class="movie-poster"
                     src="https://image.tmdb.org/t/p/w600_and_h900_bestv2/30oXQKwibh0uANGMs0Sytw3uN22.jpg"/>
                <h1>{{$movie->name}}</h1>
                <span class="minutes"><i class="fas fa-clock"></i> {{$movie->move_duration}}Minute</span>
                <p class="type">Action, Adventure, Sci-Fi</p>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6">
                            <p style="color: white"><span style="color: white">Directed by:</span>
                                @foreach($movie->directors as $directors)

                                    {{$directors->name}}
                                @endforeach</p>
                        </div>
                        <div class="col-xs-6">
                            <p style="color: white"><span style="color: white">Actors:</span>
                                @foreach($movie->actors as $directors)

                                    {{$directors->name}} ,
                                @endforeach</p>
                        </div>
                    </div>
                </div>


            </div>
            <div class="movie_desc">
                <p class="text">
                    {{$movie->desc}}
                </p>


            </div>
            <a href="#" class="trailer"><i class="fas fa-video"></i> Watch Trailer</a>
            <button type="button" class=" buy-ticket btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-ticket-alt"></i> Buy Ticket</button>


        </div>
    </div>

    <div class="container">
        <div class="be-comment-block">
            <h1 class="comments-title">{{@count($comment) }} comments</h1>
            @foreach($comment as $comment)
                <div class="be-comment">
                    <div class="be-img-comment">
                        <a href="blog-detail-2.html">
                            <img src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="" class="be-ava-comment">
                        </a>
                    </div>
                    <div class="be-comment-content">

				<span class="be-comment-name">
					<a href="blog-detail-2.html">{{$comment->user->name}}</a>
					</span>
                        <span class="be-comment-time">
					<i class="fa fa-clock-o"></i>
					{{ $comment->created_at->diffForHumans() }}
				</span>

                        <p class="be-comment-text">
                            {{$comment->body}}
                        </p>

                    </div>
                </div>
            @endforeach
            <form class="form-block" action="{{route('comment.add',$movie->id)}}">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <textarea class="form-input" required="" placeholder="Your text" name="body"></textarea>
                        </div>
                    </div>
                    <button class="btn btn-primary pull-right" type="submit">submit</button>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('scripts')
    <script type="text/javascript">
        $("#input-id").rating();


    </script>
    <script src="https://code.jquery.com/jquery-1.12.3.min.js"
            integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ="
            crossorigin="anonymous"></script>
    <script
        src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
    <script>
        $(function() {
            $('form.require-validation').bind('submit', function(e) {
                var $form         = $(e.target).closest('form'),
                    inputSelector = ['input[type=email]', 'input[type=password]',
                        'input[type=text]', 'input[type=file]',
                        'textarea'].join(', '),
                    $inputs       = $form.find('.required').find(inputSelector),
                    $errorMessage = $form.find('div.error'),
                    valid         = true;
                $errorMessage.addClass('hide');
                $('.has-error').removeClass('has-error');
                $inputs.each(function(i, el) {
                    var $input = $(el);
                    if ($input.val() === '') {
                        $input.parent().addClass('has-error');
                        $errorMessage.removeClass('hide');
                        e.preventDefault(); // cancel on first error
                    }
                });
            });
        });
        $(function() {
            var $form = $("#payment-form");
            $form.on('submit', function(e) {
                if (!$form.data('cc-on-file')) {
                    e.preventDefault();
                    Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                    Stripe.createToken({
                        number: $('.card-number').val(),
                        cvc: $('.card-cvc').val(),
                        exp_month: $('.card-expiry-month').val(),
                        exp_year: $('.card-expiry-year').val()
                    }, stripeResponseHandler);
                }
            });
            function stripeResponseHandler(status, response) {
                if (response.error) {
                    $('.error')
                        .removeClass('hide')
                        .find('.alert')
                        .text(response.error.message);
                } else {
                    // token contains id, last4, and card type
                    var token = response['id'];
                    // insert the token into the form so it gets submitted to the server
                    $form.find('input[type=text]').empty();
                    $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                    $form.get(0).submit();
                }
            }
        })
    </script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@endsection
