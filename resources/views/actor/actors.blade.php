@extends('layouts.master')
@section('metaDesc')
@endsection

@section('links')

@endsection
@section('pageName') @lang('pagesname.website') @endsection

@section('body')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
            <li class="breadcrumb-item"><a href="movies.html">Actors</a></li>
        </ol>
    </nav>
    <div class="container">
        <div class="card-group">
            @foreach($directors as $director)

                <div class="card" style="margin: 8px">
                    <img class="card-img-top" src="{{asset('img/director.png')}}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">{{$director->name}}</h5>
                        <p class="card-text"> {{$director->country}}</p>
                        <p class="card-text">Movies: {{$director->movie->name}}</p>
                        <p class="card-text"><small class="text-muted">{{$director->created_at->diffForHumans()}}</small></p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>


@endsection
@section('scripts')
    <script type="text/javascript">

    </script>
@endsection

