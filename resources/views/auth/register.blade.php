@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Register</li>
        </ol>
    </nav>
    <div class="container">
        <form method="POST" action="{{ route('register') }}">
            <div class="row">
                <div class="col-md-6 ">
                    <div id="letstalk " class="col-sm-12">

                        <div class="card-body">

                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror"
                                           name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email"
                                       class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control @error('email') is-invalid @enderror"
                                           name="email" value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                           class="form-control @error('password') is-invalid @enderror" name="password"
                                           required autocomplete="new-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="renting_freq" class="col-md-4 col-form-label text-md-right">{{ __('Renting Freq') }}</label>

                                <div class="col-md-6">
                                    <select name="renting_freq" id="enquiry" class="form-control" required="required" aria-required="true">
                                        <option value="Rarely">Rarely</option>
                                        <option value="Never">Never</option>
                                        <option value="Monthly">Monthly</option>
                                        <option value="Weekly">Weekly</option>
                                    </select>

                                    @error('renting_freq')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="viewing_freq" class="col-md-4 col-form-label text-md-right">{{ __('Viewing Freq') }}</label>

                                <div class="col-md-6">
                                    <select name="viewing_freq" id="enquiry" class="form-control" required="required" aria-required="true">
                                        <option value="Rarely">Rarely</option>
                                        <option value="Never">Never</option>
                                        <option value="Monthly">Monthly</option>
                                        <option value="Weekly">Weekly</option>
                                    </select>

                                    @error('viewing_freq')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="threat_freq" class="col-md-4 col-form-label text-md-right">{{ __('Theater Freq') }}</label>

                                <div class="col-md-6">
                                    <select name="threat_freq" id="enquiry" class="form-control" required="required" aria-required="true">
                                        <option value="Rarely">Rarely</option>
                                        <option value="Never">Never</option>
                                        <option value="Monthly">Monthly</option>
                                        <option value="Weekly">Weekly</option>
                                    </select>

                                    @error('threat_freq')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="tv_movie_freq" class="col-md-4 col-form-label text-md-right">{{ __('TV Movie Freq') }}</label>

                                <div class="col-md-6">
                                    <select name="tv_movie_freq" id="enquiry" class="form-control" required="required" aria-required="true">
                                        <option value="Rarely">Rarely</option>
                                        <option value="Never">Never</option>
                                        <option value="Monthly">Monthly</option>
                                        <option value="Weekly">Weekly</option>
                                    </select>

                                    @error('tv_movie_freq')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="tv_signal" class="col-md-4 col-form-label text-md-right">{{ __('TV Signal') }}</label>

                                <div class="col-md-6">
                                    <select name="tv_signal" id="enquiry" class="form-control" required="required" aria-required="true">
                                        <option value="Cable">Cable</option>
                                        <option value="Digital Satellite">Digital Satellite</option>
                                        <option value="Analog antenna">Analog antenna</option>
                                        <option value="Don't watch TV">Don't watch TV</option>
                                    </select>

                                    @error('tv_signal')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="martial_status" class="col-md-4 col-form-label text-md-right">{{ __('Marital Status') }}</label>

                                <div class="col-md-6">
                                    <select name="martial_status" id="enquiry" class="form-control" required="required" aria-required="true">
                                        <option value="Married">Married</option>
                                        <option value="Divorced">Divorced</option>
                                        <option value="Never Married">Never Married</option>
                                        <option value="Other">Other</option>
                                    </select>

                                    @error('martial_status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="movie_selector" class="col-md-4 col-form-label text-md-right">{{ __('Movie Selector') }}</label>

                                <div class="col-md-6">
                                    <select name="movie_selector" id="enquiry" class="form-control" required="required" aria-required="true">
                                        <option value="Spouse/Partner">Spouse/Partner</option>
                                        <option value="Me">Me</option>
                                        <option value="Other">Other</option>
                                    </select>

                                    @error('movie_selector')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="ppv_freq" class="col-md-4 col-form-label text-md-right">{{ __('PPV Freq') }}</label>

                                <div class="col-md-6">
                                    <select name="ppv_freq" id="enquiry" class="form-control" required="required" aria-required="true">
                                        <option value="Rarely">Rarely</option>
                                        <option value="Never">Never</option>
                                        <option value="Monthly">Monthly</option>
                                        <option value="Weekly">Weekly</option>
                                    </select>

                                    @error('ppv_freq')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="buying_freq" class="col-md-4 col-form-label text-md-right">{{ __('Buying Freq') }}</label>

                                <div class="col-md-6">
                                    <select name="buying_freq" id="enquiry" class="form-control" required="required" aria-required="true">
                                        <option value="Rarely">Rarely</option>
                                        <option value="Never">Never</option>
                                        <option value="Monthly">Monthly</option>
                                        <option value="Weekly">Weekly</option>
                                    </select>

                                    @error('buying_freq')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div id="letstalk " class="col-sm-12">

                        <div class="card-body">

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('date of birth') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="date"
                                           class="form-control @error('dob') is-invalid @enderror" name="dob"
                                           value="{{ old('dob') }}" required autocomplete="name" autofocus>

                                    @error('dob')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Num Bathrooms') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('num_of_bathrooms') is-invalid @enderror" name="num_of_bathrooms"
                                           value="{{ old('num_of_bathrooms') }}" required autocomplete="name" autofocus>

                                    @error('num_of_bathrooms')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Num Bedrooms') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('num_of_bedrooms') is-invalid @enderror" name="num_of_bedrooms"
                                           value="{{ old('num_of_bedrooms') }}" required autocomplete="name" autofocus>

                                    @error('num_of_bedrooms')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Num Cars') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('num_of_cars') is-invalid @enderror" name="num_of_cars"
                                           value="{{ old('num_of_cars') }}" required autocomplete="name" autofocus>

                                    @error('num_of_cars')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Num Children') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('num_of_children') is-invalid @enderror" name="num_of_children"
                                           value="{{ old('num_of_children') }}" required autocomplete="name" autofocus>

                                    @error('num_of_children')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Num TVs') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('num_of_tvs') is-invalid @enderror" name="num_of_tvs"
                                           value="{{ old('num_of_tvs') }}" required autocomplete="name" autofocus>

                                    @error('num_of_tvs')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="educational_level" class="col-md-4 col-form-label text-md-right">{{ __('Education Level') }}</label>

                                <div class="col-md-6">
                                    <select name="educational_level" id="enquiry" class="form-control" required="required" aria-required="true">
                                        <option value="Doctorate">Doctorate</option>
                                        <option value="Bachelor's Degree">Bachelor's Degree</option>
                                        <option value="Master's Degree">Master's Degree</option>
                                        <option value="Associate's Degree">Associate's Degree</option>
                                        <option value="Some College">Some College</option>
                                    </select>

                                    @error('educational_level')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

                                <div class="col-md-6">
                                    <select name="gender" id="enquiry" class="form-control" required="required" aria-required="true">
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>

                                    @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="home_ownership" class="col-md-4 col-form-label text-md-right">{{ __('Home Ownership') }}</label>

                                <div class="col-md-6">
                                    <select name="home_ownership" id="enquiry" class="form-control" required="required" aria-required="true">
                                        <option value="Own">Own</option>
                                        <option value="Rent">Rent</option>
                                    </select>

                                    @error('home_ownership')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="internet_connection" class="col-md-4 col-form-label text-md-right">{{ __('Internet Connection') }}</label>

                                <div class="col-md-6">
                                    <select name="internet_connection" id="enquiry" class="form-control" required="required" aria-required="true">
                                        <option value="Dial-Up">Dial-Up</option>
                                        <option value="DSL">DSL</option>
                                        <option value="Cable Modem">Cable Modem</option>
                                        <option value="No Internet Connection">No Internet Connection</option>
                                    </select>

                                    @error('internet_connection')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row">
                                <label for="format" class="col-md-4 col-form-label text-md-right">{{ __('Format') }}</label>

                                <div class="col-md-6">
                                    <select name="format" id="enquiry" class="form-control" required="required" aria-required="true">
                                        <option value="DVD">DVD</option>
                                        <option value="VHS">VHS</option>
                                        <option value="Betamax">Betamax</option>
                                        <option value="Other">Other</option>
                                    </select>

                                    @error('format')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <div class="form-group row mb-0" align="left">
                <div class="col-md-6 offset-md-6">
                    <button  style="width: 40vh" type="submit" class="btn btn-primary" >
                        {{ __('Register') }}
                    </button>
                </div>
            </div>
            <br><br>
            <br><br>
        </form>
    </div>
@endsection
