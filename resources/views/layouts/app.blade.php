@extends('layouts.master')
@section('metaDesc')
@endsection

@section('links')
    <style>

    </style>
@endsection
@section('pageName') @lang('pagesname.website') @endsection

@section('body')
    <div id="app">
        <main>
            @yield('content')
        </main>
    </div>
@endsection
@section('scripts')
    <script>

    </script>
@endsection
