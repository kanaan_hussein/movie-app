<style>


</style>
<nav class="navbar navbar-expand-md">
    <a class="navbar-brand" href="/"><img src="{{asset('img/logo.png')}}" /></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation"><i class="fas fa-bars"></i>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/home"><i class="fas fa-home"></i> Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/"><i class="fas fa-film"></i> Movies</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-crown"></i> Category</a>
                <div class="dropdown-menu">
                    @foreach(\App\Models\Category::all() as $category)
                    <a class="dropdown-item" href="{{route('movie.category',$category->id)}}">{{$category->name}}</a>
                    @endforeach
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('directors.show')}}"><i class="fas fa-clock"></i> Directors</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('actors.show')}}"><i class="fas fa-clock"></i> Actors</a>
            </li>

        </ul>
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest
                @if (Route::has('login'))
                    <li class="nav-item">
                        <a class=" nav-link" href="{{ route('login') }}"><span style="color: white">{{ __('Login') }}</span></a>
                    </li>
                @endif

                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class=" nav-link" href="{{ route('register') }}"><span style="color: white">{{ __('Register') }}</span></a>
                    </li>
                @endif
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
            <li>Student==> &nbsp;</li>
            <li style="color: white">kanaan_146731</li> &nbsp;||&nbsp;
            <li style="color: white">hamza_130461</li>
        </ul>
    </div>
</nav>
