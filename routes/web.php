<?php

use Illuminate\Support\Facades\Route;
use Symfony\Component\Console\Input\Input;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/', [App\Http\Controllers\HomeController::class, 'welcome_index'])->name('welcome');
Route::get('/movie/{id}', [App\Http\Controllers\MovieController::class, 'show'])->name('movie.movie');
Route::post('/movie', [App\Http\Controllers\MovieController::class, 'postPost'])->name('movie.post');

Route::get('/comment/{id}',[\App\Http\Controllers\CommentController::class,'store'])->name('comment.add');
Route::get('/category/{id}',[\App\Http\Controllers\MovieController::class,'show_category'])->name('movie.category');

Route::get('/actors',[\App\Http\Controllers\ActorController::class,'index'])->name('actors.show');
Route::get('/actor/{id}',[\App\Http\Controllers\ActorController::class,'show'])->name('actor.show');

Route::get('/directors',[\App\Http\Controllers\DirectorController::class,'index'])->name('directors.show');
Route::get('/director/{id}',[\App\Http\Controllers\DirectorController::class,'show'])->name('director.show');
Route::post('/search',[\App\Http\Controllers\MovieController::class,'search'])->name('movie.search');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
