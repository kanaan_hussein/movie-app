<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->date('dob');
            $table->string('educational_level')->nullable();
            $table->string('gender')->nullable();
            $table->string('home_ownership')->nullable();
            $table->string('internet_connection')->nullable();
            $table->string('martial_status')->nullable();
            $table->string('movie_selector')->nullable();
            $table->decimal('num_of_bathrooms')->nullable();
            $table->tinyInteger('num_of_bedrooms')->nullable();
            $table->integer('num_of_cars')->nullable();
            $table->integer('num_of_children')->nullable();
            $table->integer('num_of_tvs')->nullable();
            $table->string('ppv_freq')->nullable();
            $table->string('buying_freq')->nullable();
            $table->string('format')->nullable();
            $table->string('renting_freq')->nullable();
            $table->string('viewing_freq')->nullable();
            $table->string('threat_freq')->nullable();
            $table->string('tv_movie_freq')->nullable();
            $table->string('tv_signal')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
