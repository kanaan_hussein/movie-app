<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use willvincent\Rateable\Rateable;

class Movie extends Model
{
    use HasFactory, Rateable;
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function directors(){
        return $this->hasMany(Director::class);
    }
    public function actors(){
        return $this->hasMany(Actor::class);
    }
    public function comments(){
        return $this->hasMany(Comment::class);
    }

}
