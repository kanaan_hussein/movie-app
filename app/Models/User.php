<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends \TCG\Voyager\Models\User
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'home_ownership',
        'internet_connection',
        'martial_status',
        'movie_selector',
        'num_of_bathrooms',
        'num_of_bedrooms',
        'num_of_cars',
        'num_of_children',
        'num_of_tvs',
        'ppv_freq',
        'buying_freq',
        'renting_freq',
        'viewing_freq','threat_freq',
        'tv_movie_freq',
        'tv_signal',
        'gender',
        'educational_level',
        'dob',
        'format'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function comments(){
        return $this->hasMany(Comment::class);
    }
}
