<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'home_ownership' => $data['home_ownership'],
            'internet_connection' => $data['internet_connection'],
            'martial_status' => $data['martial_status'],
            'movie_selector' => $data['movie_selector'],
            'num_of_bathrooms' => $data['num_of_bathrooms'],
            'num_of_bedrooms' => $data['num_of_bedrooms'],
            'num_of_cars' => $data['num_of_cars'],
            'num_of_children' => $data['num_of_children'],
            'num_of_tvs' => $data['num_of_tvs'],
            'ppv_freq' => $data['ppv_freq'],
            'buying_freq' => $data['buying_freq'],
            'format' => $data['format'],
            'renting_freq' => $data['renting_freq'],

            'viewing_freq' => $data['viewing_freq'],
            'threat_freq' => $data['threat_freq'],
            'tv_movie_freq' => $data['tv_movie_freq'],
            'tv_signal' => $data['tv_signal'],
            'gender' => $data['gender'],
            'educational_level' => $data['educational_level'], 'dob' => $data['dob'],
        ]);
    }
}
