<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('welcome_index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $movies = Movie::with('category')->get();;
        $movies->each(function ($movies, $key) {
            $temp = json_decode($movies->img);
            for ($i = 0; $i < count($temp); $i++) {
                $temp[$i] = asset('storage') . '/' . $temp[$i];
            }
            $movies->img = $temp;
            unset($movies->updated_at);
        });
        return view('home', compact('movies'));
    }
    public function welcome_index()
    {
        $movies = Movie::all();
        $movies->each(function ($movies, $key) {
            $temp = json_decode($movies->img);
            for ($i = 0; $i < count($temp); $i++) {
                $temp[$i] = asset('storage') . '/' . $temp[$i];
            }
            $movies->img = $temp;
            unset($movies->updated_at);
        });
        return view('welcome', compact('movies'));
    }


}
