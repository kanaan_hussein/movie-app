<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Movie;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }


    public function store(Request $request)
    {
        $this->validate($request, array(
            'body'  => 'required'
        ));
        // store in the database
        $userpost = new Comment();
        $userpost->user_id = Auth::user()->id;
        $userpost->movie_id = $request->id;
        $userpost->body = $request->body;
        $userpost->save();

        return redirect()->to('/movie/'.$request->id);

    }
}

