<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Movie;
use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class MovieController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('search','show_category');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comment::where('movie_id',$id)->with('user')->get();
        $movie= Movie::with('actors','directors')->whereIn('id', [$id])->get();
//        $movie=Movie::find($id)->with('actors')->get();
        $movie->each(function ($movie, $key) {
            $temp = json_decode($movie->img);
            for ($i = 0; $i < count($temp); $i++) {
                $temp[$i] = asset('storage') . '/' . $temp[$i];
            }
            $movie->img = $temp;
        });
        $movie=$movie[0];


        return  view('movie.movie',compact('movie','comment'));
    }
   public function search( Request $request){

       $q =$request->input('search');
        $user = \App\Models\Movie::where('name','LIKE','%'.$q.'%')->orWhereHas('actors', function ($query) use($q) {
            $query->where('name', '=', $q);
        })
            ->orWhere('desc','LIKE','%'.$q.'%')
            ->orWhere('move_duration','LIKE','%'.$q.'%')
            ->orWhere('year','LIKE','%'.$q.'%')->
            get();
       $user->each(function ($user, $key) {
           $temp = json_decode($user->img);
           for ($i = 0; $i < count($temp); $i++) {
               $temp[$i] = asset('storage') . '/' . $temp[$i];
           }
           $user->img = $temp;
       });
        if(count($user) > 0)

            return view('search')->withDetails($user)->withQuery ( $q );
        else return view ('search')->withMessage('No Details found. Try to search again !');
    }
    public function show_category($id)
    {

        $movie=Movie::where('cat_id',$id)->with('category')->get();
        $movie->each(function ($movie, $key) {
            $temp = json_decode($movie->img);
            for ($i = 0; $i < count($temp); $i++) {
                $temp[$i] = asset('storage') . '/' . $temp[$i];
            }
            $movie->img = $temp;
        });


        return  view('category.movie',compact('movie',));
    }
    public function postPost(Request $request)
    {
        request()->validate(['rate' => 'required']);
        $post = Movie::find($request->id);
        $rating = new \willvincent\Rateable\Rating;
        $rating->rating = $request->rate;
        $rating->user_id = auth()->user()->id;
        $post->ratings()->save($rating);
        return redirect()->route("movie.movie",$request->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        //
    }
}
